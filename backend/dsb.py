import requests
from requests.exceptions import SSLError, ConnectionError
from bs4 import BeautifulSoup
import os
import re
import time
from datetime import datetime
import json
import gzip
import base64
from jsondiff import diff, insert, delete
from dotenv import load_dotenv
from urllib.parse import urlparse
from pywebpush import webpush, WebPushException


def get_plan_urls(username, password):
    date_string = datetime.now().strftime("%Y-%m-%dT%H:%M:%S:%f0")
 
    data = json.dumps({
        "AppId": "2092d12c-8470-4ef8-8c70-584965f9ffe5", # any AppId in 00000000-0000-0000-0000-000000000000 format possible?
        "UserId": username,
        "UserPw": password,
        "AppVersion": "2.5.9", # latest in PlayStore since 2016/02/18
        "Device": "VirtualBox", # optional?
        "OsVersion": "27 8.1.0", # optional?
        "Language": "de", # optional?
        "Date": date_string,
        "LastUpdate": date_string,
        "BundleId": "de.heinekingmedia.dsbmobile" # anything possible?
    }, separators=(",", ":")).encode("utf-8")
    
    body = {
        "req": {
            "Data": base64.b64encode(gzip.compress(data)).decode("utf-8"), # data is sent gzip compressed and base64 encoded
            "DataType": 1
        }
    }
    
    try:
        response = requests.post("https://app.dsbcontrol.de/JsonHandler.ashx/GetData", json=body)
        response_data = json.loads(gzip.decompress(base64.b64decode(response.json()["d"])).decode("utf-8"))
        for result in response_data['ResultMenuItems']:
            if result['Title'] != 'Inhalte':
                continue
            for child in result['Childs']:
                if child['MethodName'] != 'timetable':
                    continue
                for child2 in child['Root']['Childs']:
                    if child2['Title'] != 'Schüler mobil':
                        continue
                    return list(map(lambda plan: plan['Detail'], child2['Childs']))
    except TimeoutError | urllib3.exceptions.NewConnectionError | urllib3.exceptions.MaxRetryError | requests.exceptions.ConnectionError as e:
        print(e)
    return []


def get_subst(url):
    resp = {}
    try:
        r = requests.get(url)
        soup = BeautifulSoup(r.text, "html.parser")

        date_string = soup.select_one(".mon_title").text.split(" ")
        week_day = date_string[1]
        date_format = "%d.%m.%Y"
        date = datetime.strptime(date_string[0], date_format)

        head_info = soup.select_one(".mon_head p").text
        updated_start = "Stand: "

        resp.update({
            "weekDay": week_day,
            "date": date.strftime(date_format),
            "lastUpdated": head_info[head_info.find(updated_start) + len(updated_start):]
        })

        infos = []

        info_rows = soup.select("tr.info")

        for info_row in info_rows[1:]:
            cells = [c.text.replace(u"\u00A0", "") for c in info_row.select("td.info")]

            if len(cells) == 1:
                for info in cells[0].splitlines():
                    infos.append(info)
            elif len(cells) > 1:
                if cells[0] != "Betroffene Klassen":
                    infos.append(" | ".join(cells))

        resp.update({"infos": infos})

        substitutions = []

        subst_rows = soup.select("tr.list")

        for subst_row in subst_rows[1:]:
            cells = [c.text for c in subst_row.select("td.list")]
            classes = cells[0].replace(" ", "").split(",")

            for c in classes:
                # if student is in 11, 12 or 13: don't name the key "subject", but "course"
                grade_numbers = re.search(r'\d+', c)
                grade = int(grade_numbers.group(0)) if grade_numbers != None else None
                substitutions.append({
                    "class": c.lstrip('0'), # workaround to ignore leading zeros for classes 5-9 by removing them
                    "lesson": cells[1],
                    "type": cells[6],
                    f"{'subject' if grade == None or grade < 11 else 'course'}": cells[2].replace("---", "-").strip() + ";" + cells[4].replace("---", "-").strip(),
                    "teacher": cells[3].replace("---", "-").replace("+", "-"),
                    "room": cells[5].replace("---", "-"),
                    "note": "-" if cells[7].strip() == "" else cells[7]
                })

        resp.update({"substitutions": substitutions})
    except (SSLError, ConnectionError) as e:
        print(e)
    # in case of error, the empty object is returned
    return resp


plans_directory_name = "plans"
if not os.path.exists(plans_directory_name):
    os.makedirs(plans_directory_name)

old_jsons = {}
old_file_names = [plans_directory_name + "/" + fn for fn in os.listdir(plans_directory_name) if fn.endswith(".json")]
for file_name in old_file_names:
    with open(file_name, "r+", encoding="utf-8") as f:
        # if file is empty
        if not f.read(1):
            continue
        # go back to start of file
        f.seek(0)
        # read file content as json
        old_json = json.loads(f.read())
        old_jsons[old_json["date"]] = old_json

load_dotenv(dotenv_path="../frontend/.env")
username = os.getenv("LOGIN_USERNAME")
password = os.getenv("LOGIN_PASSWORD")
vapid_private_key = os.getenv("PRIVATE_VAPID_KEY")

plan_urls = get_plan_urls(username, password)

new_jsons = {}
new_file_names = []
for url in plan_urls:
    file_name = plans_directory_name + "/" + url.split("/")[-1][:-4] + ".json"
    new_file_names.append(file_name)

    new_json = get_subst(url)
    if not new_json:  # there must've occurred an error in the request
        continue
    new_jsons[new_json["date"]] = new_json
    
    with open(file_name, "w+", encoding="utf-8") as f:
        f.write(json.dumps(new_json, ensure_ascii=False))

for old_file_name in old_file_names:
    if old_file_name not in new_file_names:
        open(old_file_name, 'w').close()  # clear file

time_in_millis = int(round(time.time() * 1000))

categories = ["infos", "substitutions"]
changes = {}
change_amount = 0

for new_date, new_json in new_jsons.items():
    changes[new_date] = {
        "inserted": {
            "infos": [],
            "substitutions": []
        },
        "updated": {
            "infos": [],
            "substitutions": []
        },
        "deleted": {
            "infos": [],
            "substitutions": []
        }
    }

    # completely new day
    if new_date not in old_jsons:
        for category in categories:
            changes[new_date]["inserted"][category] = new_json[category]
            change_amount += len(new_json[category])
        continue

    # old, but possibly changed day

    old_json = old_jsons[new_date]
    
    diff_dict = diff(old_json, new_json, syntax="symmetric")

    for category in categories:
        if category in diff_dict:
            data = diff_dict[category]

            if insert in data:
                for new_data in [new_json[category][int(index)] for (index, _) in data[insert]]:
                    changes[new_date]["inserted"][category].append(new_data)
                    change_amount += 1

            if category == "infos":  # infos (list of updates)
                updated_infos = data[1]
                for updated_data in updated_infos:
                    changes[new_date]["updated"][category].append(updated_data)
                    change_amount += 1
            else:  # substitutions (dict of updates)
                updated_indices = filter(lambda key: isinstance(key, int), data.keys())
                for updated_data in [new_json[category][int(index)] for index in updated_indices]:
                    changes[new_date]["updated"][category].append(updated_data)
                    change_amount += 1

            if delete in data:
                for deleted_data in [old_json[category][int(index)] for (index, _) in data[delete]]:
                    changes[new_date]["deleted"][category].append(deleted_data)
                    change_amount += 1


def is_substitution_valid(substitution):
    if class_["selectedGrade"] < 11:
        return substitution["class"] == str(class_["selectedGrade"]) + class_["selectedClass"]
    else:
        if "subject" in substitution:
            # in 11, 12 and 13: 'subject' is named 'course'
            return False
        old_course, new_course = substitution["course"].split(";")
        course = old_course if not old_course else new_course
        return substitution["class"] == str(class_["selectedGrade"]) and course in class_["selectedCourses"]


if change_amount > 0:
    sessions_directory_name = "../frontend/sessions"
    for file_name in [sessions_directory_name + "/" + fn for fn in os.listdir(sessions_directory_name) if fn.endswith(".json")]:
        with open(file_name, "r+", encoding="utf-8") as f:
            # if file is empty
            if not f.read(1):
                continue
            # go back to start of file
            f.seek(0)
            # read file content as json
            session = json.loads(f.read())

            if "authUser" not in session:
                continue
            auth_user = session["authUser"]
            if auth_user["username"] != username or auth_user["password"].lower() != password.lower():
                continue
                
            if "class_" not in session:
                continue
            class_ = session["class_"]
            if class_ is None or class_["selectedGrade"] is None:
                continue
            
            change_messages = []
            for plan_date, diff_json in changes.items():
                for change_type, type_translation in {
                    "inserted": {
                        "info": "",
                        "substitution": "neu"
                    },
                    "updated": {
                        "info": "(geändert) ",
                        "substitution": "geändert"
                    },
                    "deleted": {
                        "info": "(entfernt) ",
                        "substitution": "entfernt"
                    }
                }.items():
                    infos = diff_json[change_type]["infos"]
                    for info in infos:
                        change_messages.append("{}{} ({})".format(type_translation["info"], info, plan_date))
                        
                    substitutions = list(filter(is_substitution_valid, diff_json[change_type]["substitutions"]))
                    substitution_amount = len(substitutions)
                    if substitution_amount > 0:
                        change_messages.append("{} {}e Vertretung{} am {}".format(substitution_amount, type_translation["substitution"], "en" if substitution_amount > 1 else "", plan_date))
            
            if len(change_messages) == 0:
                continue

            if "subscription" not in session:
                continue
            subscription = session["subscription"]
            if subscription is None:
                continue
            
            data = {
                "title": "Es gibt Neuigkeiten für Dich!",
                "body": "\n".join(change_messages),
                "icon": "https://wetter.codes/gmo/icon.png",
                "timestamp": time_in_millis
            }
            vapid_claims = {
                "aud": "{uri.scheme}://{uri.netloc}".format(uri=urlparse(subscription["endpoint"])),
                "sub": "mailto:contact@wetter.codes"
            }
            try:
                webpush(
                    subscription_info=subscription,
                    data=json.dumps(data),
                    vapid_private_key=vapid_private_key,
                    vapid_claims=vapid_claims
                )
            except WebPushException as e:
                if 'invalid push subscription endpoint' in str(e):
                    session["subscription"] = None
                    f.seek(0)
                    json.dump(session, f, indent=2)
                    f.truncate()

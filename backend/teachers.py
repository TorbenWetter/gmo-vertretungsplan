import requests
from bs4 import BeautifulSoup
import json

hours = requests.get("https://www.gymnasium-oberstadt.de/index.php?seite=33.sprechstundenundmailadressen")
soup = BeautifulSoup(hours.text, "html.parser")

teachers = {}
for row in soup.find("table").find("tbody").find_all("tr"):
    if row.find("th"):
        # head row
        continue
    
    data = row.find_all("td")
    initials, name = [td.text for td in data[:2]]
    teachers[initials] = name

with open("teachers.json", "w+", encoding="utf8") as teachers_file:
    json.dump(teachers, teachers_file, ensure_ascii=False)

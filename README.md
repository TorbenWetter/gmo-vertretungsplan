# GMO-Vertretungsplan

> Applikation zum Erfragen von Teilen des Vertretungsplans

## Build Setup

``` bash
# Go to backend/
$ cd backend/

# Create and activate virtual environment
$ python3.7 -m venv .venv
$ source .venv/bin/activate

# Install required python libraries
$ python3.7 -m pip install -r requirements.txt

# Execute dsb and teacher scripts in backend
$ python3.7 dsb.py
$ python3.7 teachers.py

# Get .env file and move it to frontend/
# Go to frontend/
$ cd ../frontend/

# Install dependencies
$ npm install

# Serve with hot reload at localhost:3000
$ npm run dev

# Build for production and launch server
$ npm run build
$ npm run start

# Generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

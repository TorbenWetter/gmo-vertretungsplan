module.exports = {
  mode: 'universal',
  router: {
    base: '/gmo/'
  },
  serverMiddleware: ['~/api/index.js'],
  env: {
    publicVapidKey: process.env.PUBLIC_VAPID_KEY
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'GMO-Vertretungsplan', // doesn't seem to do anything
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/eslint-module',
    [
      'nuxt-fontawesome', {
        component: 'fa',
        imports: [
          {
            set: '@fortawesome/free-solid-svg-icons',
            icons: ['fas']
          }
        ]
      }
    ]
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: (process.env.NODE_ENV === 'production' ? 'https://wetter.codes' : 'http://localhost:3000') + '/gmo/api'
  },
  pwa: {
    workbox: {
      swURL: 'worker.js'
    },
    manifest: {
      name: 'GMO-Vertretungsplan',
      short_name: 'GMO-Vertretungsplan',
      start_url: '/gmo/?standalone=true',
      display: 'standalone',
      orientation: 'portrait',
      theme_color: '#dcedc8',
      lang: 'de'
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
};

import Vue from 'vue';
import deepEqual from 'deep-equal';

const defaultState = {
  authUser: null,
  class_: {
    selectedGrade: null,
    selectedClass: null,
    selectedCourses: []
  },
  subscription: null
};

export const state = () => Vue.util.extend({}, defaultState);

export const mutations = {
  RESET_STATE (state) {
    for (const key in state) {
      // if value is not an object or was null initially
      if (typeof state[key] !== 'object' || defaultState[key] === null) {
        // then just reset it to default value
        Vue.set(state, key, defaultState[key]);
        continue;
      }

      // if value is an object,
      // reset it in two dimensions (would have to be extended for 3rd, 4th, ..)
      for (const innerKey in state[key]) {
        Vue.set(state[key], innerKey, defaultState[key][innerKey]);
      }
    }
  },
  SET_USER (state, user) {
    state.authUser = user;
  },
  SET_CLASS (state, class_) {
    state.class_ = class_;
  },
  SET_SUBSCRIPTION (state, subscription) {
    state.subscription = subscription;
  },
  UNSET_SUBSCRIPTION (state) {
    state.subscription = null;
  }
};

export const actions = {
  nuxtServerInit ({ commit }, { req }) {
    // Data from server
    if (req.session) {
      if (req.session.authUser) {
        // Client side
        commit('SET_USER', req.session.authUser);
      }
      if (req.session.class_) {
        // Client side
        commit('SET_CLASS', req.session.class_);
      }
      if (req.session.subscription) {
        // Client side
        commit('SET_SUBSCRIPTION', req.session.subscription);
      }
      // other (initial) parts of the state have to be added here
    }
  },
  async login ({ commit }, { username, password }) {
    try {
      const res = await this.$axios.post('/login', {
        username,
        password
      }, {
        headers: {
          'Content-Type': 'application/json'
        },
        withCredentials: true,
        credentials: 'same-origin'
      });

      if (res.status === 200) {
        // Client side
        commit('SET_USER', res.data.user);
        return true;
      }
    } catch (error) {
    }
    return false;
  },
  async logout ({ commit }) {
    try {
      const res = await this.$axios.post('/logout', {
        withCredentials: true,
        credentials: 'same-origin'
      });

      if (res.status === 200) {
        // Client side
        commit('RESET_STATE');
        return true;
      }
    } catch (error) {
    }
    return false;
  },
  async setup ({ commit }, { selectedGrade, selectedClass, selectedCourses }) {
    try {
      const res = await this.$axios.post('/setup', {
        class_: {
          selectedGrade,
          selectedClass,
          selectedCourses
        }
      }, {
        headers: {
          'Content-Type': 'application/json'
        },
        withCredentials: true,
        credentials: 'same-origin'
      });

      if (res.status === 200) {
        // Client side
        commit('SET_CLASS', res.data.class_);
        return true;
      }
    } catch (error) {
    }
    return false;
  },
  async getPlans ({ commit }) {
    try {
      const res = await this.$axios.post('/get_plans', {
        withCredentials: true,
        credentials: 'same-origin'
      });

      if (res.status === 200) {
        return res.data.plans;
      }

      // store is set but session is probably not -> delete store
      if (res.status === 401) {
        commit('RESET_STATE');
      }
    } catch (error) {
    }
    return [];
  },
  async setNotificationSubscription ({ commit }, { subscription }) {
    try {
      const res = await this.$axios.post('/subscribe', {
        subscription: JSON.stringify(subscription)
      }, {
        headers: {
          'Content-Type': 'application/json'
        },
        withCredentials: true,
        credentials: 'same-origin'
      });

      if (res.status === 200) {
        // Client side
        commit('SET_SUBSCRIPTION', res.data.subscription);
        return true;
      }
    } catch (error) {
    }
    return false;
  },
  async unsetNotificationSubscription ({ commit }) {
    try {
      const res = await this.$axios.post('/unsubscribe', {
        withCredentials: true,
        credentials: 'same-origin'
      });

      if (res.status === 200) {
        // Client side
        commit('UNSET_SUBSCRIPTION');
        return true;
      }
    } catch (error) {
    }
    return false;
  },
  async notificationsSubscribed ({ commit }) {
    try {
      const res = await this.$axios.post('/subscribed', {
        withCredentials: true,
        credentials: 'same-origin'
      });

      if (res.status === 200) {
        return res.data.subscribed;
      }
    } catch (error) {
    }
    return false;
  },
  classDiffers ({ commit }, { class_ }) {
    return !deepEqual(this.state.class_, class_);
  }
};

const fs = require('fs');
const path = require('path');
const express = require('express');
const app = express();

require('dotenv/config');
const cookieName = process.env.COOKIE_NAME;
const username = process.env.LOGIN_USERNAME;
const password = process.env.LOGIN_PASSWORD;
const publicVapidKey = process.env.PUBLIC_VAPID_KEY;

// POST `/login` to log in the user and add him to the `req.session.authUser`
app.post('/login', function (req, res) {
  // password is case-insensitive
  if (req.body.username === username && req.body.password.toLowerCase() === password.toLowerCase()) {
    const user = {
      username,
      password
    };
    // Server side
    req.session.authUser = user;
    return res.status(200).json({ user });
  }
  return res.status(401).end();
});

// POST `/logout` to log out the user and remove it from the `req.session`
app.post('/logout', function (req, res) {
  // Server side
  req.session.destroy(err => {
    if (err) {
      return res.status(500).end();
    } else {
      return res.status(200).clearCookie(cookieName).end();
    }
  });
});

app.post('/setup', function (req, res) {
  // Server side
  const class_ = req.body.class_;
  req.session.class_ = class_;
  return res.status(200).json({ class_ });
});

app.post('/get_plans', function (req, res) {
  // Server side
  const class_ = req.session.class_;
  if (class_ == null || class_.selectedGrade == null) {
    return res.status(401).end();
  }

  let isSubstitutionValid;
  if (class_.selectedGrade < 11) {
    isSubstitutionValid = substitution => substitution.class === class_.selectedGrade.toString() + class_.selectedClass;
  } else {
    isSubstitutionValid = substitution => {
      if ('subject' in substitution) {
        // in 11, 12 and 13: 'subject' is named 'course'
        return false;
      }
      const [oldCourse, newCourse] = substitution.course.split(';');
      const course = oldCourse !== '' ? oldCourse : newCourse;
      return substitution.class === class_.selectedGrade.toString() && class_.selectedCourses.includes(course);
    };
  }

  const plans = [];
  const planPath = '../backend/plans'.replace('/', path.sep);
  fs.readdirSync(planPath).forEach(fileName => {
    const fileContent = fs.readFileSync(planPath + path.sep + fileName, 'utf8');
    if (!fileContent.trim()) {
      return;
    }
    const planData = JSON.parse(fileContent);
    planData.substitutions = planData.substitutions.filter(substitution => isSubstitutionValid(substitution));
    planData.substitutions = planData.substitutions.map(substitution => {
      delete substitution.class;
      return substitution;
    });
    plans.push(planData);
  });
  return res.status(200).json({ plans });
});

app.post('/get_teacher_names', function (req, res) {
  // Server side
  const teachersPath = '../backend/teachers.json';
  if (!fs.existsSync(teachersPath)) {
    // File does not exist..
    return res.status(500).end();
  }

  const fileContent = fs.readFileSync(teachersPath, 'utf8');
  if (!fileContent.trim()) {
    return res.status(401).end();
  }
  return res.status(200).json({
    teachers: JSON.parse(fileContent)
  });
});

app.post('/get_public_vapid_key', function (req, res) {
  return res.status(200).json({ publicVapidKey });
});

app.post('/subscribe', function (req, res) {
  const subscription = JSON.parse(req.body.subscription);
  req.session.subscription = subscription;
  res.status(200).json({ subscription });
});

app.post('/unsubscribe', function (req, res) {
  req.session.subscription = null;
  res.status(200).end();
});

app.post('/subscribed', function (req, res) {
  res.status(200).json({
    subscribed: req.session.subscription != null
  });
});

module.exports = {
  path: '/api',
  handler: app
};

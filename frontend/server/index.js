const express = require('express');
const consola = require('consola');
const { Nuxt, Builder } = require('nuxt');
const bodyParser = require('body-parser');
const session = require('express-session');
const FSStore = require('connect-fs2')(session);
const app = express();

// Body parser, to access `req.body`
app.use(bodyParser.json());

require('dotenv/config');
const cookieName = process.env.COOKIE_NAME;
const sessionSecret = process.env.SESSION_SECRET;

// Sessions to create `req.session`
app.use(session({
  name: cookieName,
  store: new FSStore({
    dir: './sessions',
    beautify: true
  }),
  secret: sessionSecret,
  resave: false,
  saveUninitialized: false,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 * 7 * 52 // 1 year
  }
}));

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js');
config.dev = !(process.env.NODE_ENV === 'production');

async function start() {
  // Log function
  var logger = function(req, res, next) {
    const { method, url } = req;
    const urlRule = /^\/gmo\/(?!api|_nuxt|__webpack_hmr|worker\.js|bootstrap|vue).*$/g;
    const match = url.match(urlRule);
    if (match && match.length > 0 && match[0] === url && method === 'GET') {
      consola.log(
        method, 
        url, 
        '|', 
        req.session && req.session.class_ && (class_ => {
          const grade = class_.selectedGrade;
          const courses = grade >= 11 ? ' (' + class_.selectedCourses + ')' : class_.selectedClass;
          return grade + courses;
        })(req.session.class_), 
        '|', 
        req.headers['user-agent']
      );
    }
    next();
  };

  // Give logger function to express
  app.use(logger);

  // Init Nuxt.js
  const nuxt = new Nuxt(config);

  const { host, port } = nuxt.options.server;

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt);
    await builder.build();
  } else {
    await nuxt.ready();
  }

  // Give nuxt middleware to express
  app.use(nuxt.render);

  // Listen the server
  app.listen(port, host);
  consola.ready({
    message: `Server listening on http://${host}:${port}${config.dev ? '/gmo' : ''}`,
    badge: true
  });
}
start();

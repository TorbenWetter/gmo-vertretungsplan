importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

const { routing, strategies, expiration } = workbox;

// Notification push event
self.addEventListener('push', event => {
  const data = event.data.json();
  event.waitUntil(
    self.registration.showNotification(data.title, {
      body: data.body,
      icon: data.icon,
      badge: data.icon,
      actions: [
        {
          action: 'details-action',
          title: 'Details sehen'
        }
      ],
      timestamp: data.timestamp,
      requireInteraction: true
    })
  );
});

// Notification click event
self.addEventListener('notificationclick', event => {
  // if (event.action && event.action === 'details-action')

  event.notification.close();

  const rootUrl = new URL('./', self.location).href; 
  event.waitUntil(
    clients.matchAll().then(matchedClients => {
      for (let client of matchedClients) {
        if (client.url.indexOf(rootUrl) > -1) {
          return client.focus();
        }
      }

      return clients.openWindow(rootUrl);
    })
  );
});

const cacheName = 'substitution-page';

// Cache the main substitution page with a network first strategy.
routing.registerRoute(
  /\/gmo\/(\?standalone=true)?$/,
  ({url, event}) => caches.open(cacheName).then(cache => {
    const strippedUrl = `${url.origin}${url.pathname}`;

    // Network first
    return fetch(event.request).then(fetchResponse => {
      // store response with stripped url
      cache.put(strippedUrl, fetchResponse.clone());
      return fetchResponse;
    }).catch(() => {
      // Cache second (with stripped url)
      return cache.match(strippedUrl);
    });
  })
);

// Cache nuxt resources in the StaleWhileRevalidate strategy
routing.registerRoute(
  /\/gmo\/_nuxt\//,
  new strategies.StaleWhileRevalidate({
    cacheName: 'nuxt-resources'
  })
);

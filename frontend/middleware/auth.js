export default function ({ store, redirect, route }) {
  if (route.path !== '/login' && store.state.authUser == null) {
    return redirect('/login');
  }

  if (route.path !== '/settings' && store.state.class_.selectedGrade == null) {
    return redirect('/settings');
  }

  const user = store.state.authUser;
  return store
    .dispatch('login', {
      username: user.username,
      password: user.password
    })
    .then(success => {
      if (!success) {
        store.dispatch('logout');
        // No callback because we just hope the logout is successful
      }
    });
}
